def average(l):
    assert len(l) > 0
    return float(sum(l)) / len(l)


class Formula():
    def __init__(self, clauses=None):
        self.clauses = clauses or list()

    def combine(self, other):
        assert isinstance(other, Formula)
        return Formula(self.clauses + other.clauses)

    def eval(self, model):
        return all([clause.eval(model) for clause in self.clauses])

    def satdelta(self, model):
        return sum([clause.satdelta(model) for clause in self.clauses])
    
    def manyModelSatdelta(self, models):
        assert models
        return average([self.satdelta(model) for model in models])

    def __repr__(self):
        if len(self.clauses) == 0:
            return ""
        elif len(self.clauses) == 1:
            return repr(self.clauses[0])
        return "(and {})".format(" ".join(map(repr, self.clauses)))


class Clause():
    def __init__(self, ineqs=None):
        self.ineqs = ineqs or list()

    def combine(self, other):
        assert isinstance(other, Clause)
        return Clause(self.ineqs + other.ineqs)

    def eval(self, model):
        return any([ineq.eval(model) for ineq in self.ineqs])

    def satdelta(self, model):
        return min([ineq.satdelta(model) for ineq in self.ineqs])

    def __repr__(self):
        if len(self.ineqs) == 0:
            return ""
        elif len(self.ineqs) == 1:
            return repr(self.ineqs[0])
        return "(or {})".format(" ".join(map(repr, self.ineqs)))


class Ineq():
    def __init__(self, lhs, rhs, comp):
        self.lhs = lhs
        self.rhs = rhs
        self.comp = comp

    def eval(self, model):
        if self.comp == "<":
            return self.lhs.eval(model) < self.rhs.eval(model)
        elif self.comp == "<=":
            return self.lhs.eval(model) <= self.rhs.eval(model)
        elif self.comp == ">":
            return self.lhs.eval(model) > self.rhs.eval(model)
        elif self.comp == ">=":
            return self.lhs.eval(model) >= self.rhs.eval(model)
        elif self.comp == "=":
            return self.lhs.eval(model) == self.rhs.eval(model)
        elif self.comp == "!=":
            return self.lhs.eval(model) != self.rhs.eval(model)

    def satdelta(self, model):
        lhs_value = self.lhs.eval(model)
        rhs_value = self.rhs.eval(model)
        
        if self.comp == "<":
            if lhs_value >= rhs_value:
                return (lhs_value - rhs_value) + 1
        elif self.comp == "<=":
            if lhs_value > rhs_value:
                return (lhs_value - rhs_value)
        elif self.comp == ">":
            if lhs_value <= rhs_value:
                return (rhs_value - lhs_value) + 1
        elif self.comp == ">=":
            if lhs_value < rhs_value:
                return (rhs_value - lhs_value)
        elif self.comp == "=":
            if lhs_value != rhs_value:
                return abs(lhs_value - rhs_value)
        elif self.comp == "!=":
            if lhs_value == rhs_value:
                return 1

        return 0

    def __repr__(self):
        return "({} {} {})".format(
            self.comp,
            repr(self.lhs),
            repr(self.rhs),
        )


class Sum():
    def __init__(self, lhs, rhs):
        self.lhs = lhs
        self.rhs = rhs

    def eval(self, model):
        return self.lhs.eval(model) + self.rhs.eval(model)

    def __repr__(self):
        return "(+ {} {})".format(repr(self.lhs), repr(self.rhs))


class Mul():
    def __init__(self, lhs, rhs):
        self.lhs = lhs
        self.rhs = rhs

    def eval(self, model):
        return self.lhs.eval(model) * self.rhs.eval(model)

    def __repr__(self):
        return "(* {} {})".format(repr(self.lhs), repr(self.rhs))


class Var():
    def __init__(self, index):
        self.index = index

    def eval(self, model):
        return model[self.index]

    def __repr__(self):
        return "x{}".format(self.index)


class Const():
    def __init__(self, val):
        self.val = val

    def eval(self, model):
        return self.val

    def __repr__(self):
        return str(self.val)


# A box is an hyperrectangle.
class Box():
    def __init__(self, coords, sizes):
        assert(len(coords) == len(sizes))
        self.coords = coords
        self.sizes = sizes
        self.n = len(coords)
    
    def toFormula(self, format=0):
        assert 0 <= format <= 2
    
        coords = self.coords
        sizes = self.sizes
    
        if format == 0:
            clauses = []
            for i in xrange(self.n):
                clauses.append(Clause([Ineq(Var(i), Const(coords[i]), ">=")]))
                clauses.append(Clause([Ineq(Var(i), Const(coords[i] + sizes[i]), "<=")]))
            return Formula(clauses)
        elif format == 1:
            clauses = []
            for i in xrange(self.n):
                clauses.append(
                    Clause([
                        Ineq(Var(0), Const(coords[i]), ">"),
                        Ineq(Var(0), Const(coords[i]), "="),
                    ])
                )
                clauses.append(
                    Clause([
                        Ineq(Var(0), Const(coords[i]+sizes[i]), "<"),
                        Ineq(Var(0), Const(coords[i]+sizes[i]), "="),
                    ])
                )
            return Formula(clauses)
        elif format == 2:
            clauses = []
            for i in xrange(self.n):
                clauses.append(Clause([Ineq(Var(0), Const(coords[i]-1), ">=")]))
                clauses.append(Clause([Ineq(Var(0), Const(coords[i]-1), "!=")]))
                clauses.append(Clause([Ineq(Var(0), Const(coords[i]+sizes[i]+1), "<=")]))
                clauses.append(Clause([Ineq(Var(0), Const(coords[i]+sizes[i]+1), "!=")]))
            return Formula(clauses)

    def __repr__(self):
        return "Box(coords={},sizes={})".format(
            self.coords,
            self.sizes,
        )


if __name__ == '__main__':
    import argparse
    import random


    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('--seed', default=0, type=int)
    parser.add_argument('--boxes', default=1000, type=int)
    parser.add_argument('--dimensions', default=2, type=int)
    parser.add_argument('--side-size', default=1000, type=int)
    args = parser.parse_args()
    
    SEED = args.seed
    NR_FORMULAS = args.boxes # Number of pairs of formulas generated per single experiment.
    DIMENSIONS = args.dimensions # Number of dimensions of the boxes.
    MAX_SIDE_SIZE = args.side_size # Maximum size of the size of the side of the box encoded by each formula.

    # Enable the replication of the experiment.
    random.seed(SEED)
    
    assert DIMENSIONS > 0
    assert NR_FORMULAS > 0
    assert MAX_SIDE_SIZE >= 3

    # Lists of models to tune satdelta.
    def kmodel(vars, k):
        """Helper function to create constant models on `vars` variables."""
        return dict(zip(xrange(vars), [k]*vars))
    
    def kmodelDIM(k):
        """Helper function to create constant models on DIM variables."""
        return kmodel(DIMENSIONS,k)
    
    MODELS = {
        "satdelta_A1": [
            kmodelDIM(0)
        ],
        "satdelta_B1": [
            kmodelDIM(2)
        ],
        "satdelta_C1": [
            kmodelDIM(1)
        ],
        "satdelta_D1": [
            kmodelDIM(-1)
        ],
        "satdelta_E1": [
            kmodelDIM(-2)
        ],
        "satdelta_A3": [
            kmodelDIM(-1),
            kmodelDIM(0),
            kmodelDIM(1),
        ],
        "satdelta_B3": [
            kmodelDIM(-1000),
            kmodelDIM(0),
            kmodelDIM(1000),
        ],
        "satdelta_C3": [
            kmodelDIM(-10000),
            kmodelDIM(0),
            kmodelDIM(100),
        ],
        "satdelta_A5": [
            kmodelDIM(-2),
            kmodelDIM(-1),
            kmodelDIM(0),
            kmodelDIM(1),
            kmodelDIM(2),
        ],
        "satdelta_B5": [
            kmodelDIM(-10000),
            kmodelDIM(-100),
            kmodelDIM(0),
            kmodelDIM(100),
            kmodelDIM(10000),
        ],
        "satdelta_A9": [
            kmodelDIM(-10000),
            kmodelDIM(-1000),
            kmodelDIM(-100),
            kmodelDIM(-10),
            kmodelDIM(0),
            kmodelDIM(10),
            kmodelDIM(100),
            kmodelDIM(1000),
            kmodelDIM(10000),
        ],
        "satdelta_B9": [
            kmodelDIM(-4),
            kmodelDIM(-3),
            kmodelDIM(-2),
            kmodelDIM(-1),
            kmodelDIM(0),
            kmodelDIM(1),
            kmodelDIM(2),
            kmodelDIM(3),
            kmodelDIM(4),
        ],
    }

    # Case 1 (perfect overlap).
    results1 = dict([(key, list()) for key in MODELS])
    for i in xrange(NR_FORMULAS):
        coords = [0]*DIMENSIONS
        sizes = [random.randint(1, MAX_SIDE_SIZE) for _ in xrange(DIMENSIONS)]
        b = Box(coords, sizes)
        
        f1 = b.toFormula(format=0)
        # f2 = r.toFormula(format=1)
        f2 = b.toFormula(format=2)

        for key, models in MODELS.items():
            sd1 = f1.manyModelSatdelta(models)
            sd2 = f2.manyModelSatdelta(models)
            results1[key].append(abs(sd1 - sd2))

    print "case-1 (perfect overlap):"
    for key, values in results1.items():
        print "  {} = {:.2f}".format(key, average(values))
    print


    # Case 2 (inclusion).
    results2 = dict([(key, list()) for key in MODELS])
    for i in xrange(NR_FORMULAS):
        coords1 = [0]*DIMENSIONS
        sizes1 = [random.randint(3, MAX_SIDE_SIZE) for _ in xrange(DIMENSIONS)]
        b1 = Box(coords1, sizes1)
        
        coords2 = [random.randint(1, sizes1[i]-2) for i in xrange(DIMENSIONS)]
        sizes2 = [random.randint(1, sizes1[i]-coords2[i]-1) for i in xrange(DIMENSIONS)]
        b2 = Box(coords2, sizes2)
        
        f1 = b1.toFormula()
        f2 = b2.toFormula()

        for key, models in MODELS.items():
            sd1 = f1.manyModelSatdelta(models)
            sd2 = f2.manyModelSatdelta(models)
            results2[key].append(abs(sd1 - sd2))

    print "case-2 (inclusion):"
    for key, values in results2.items():
        print "  {} = {:.2f}".format(key, average(values))
    print


    # Case 3 (partial overlap).
    results3 = dict([(key, list()) for key in MODELS])
    for i in xrange(NR_FORMULAS):
        coords1 = [0]*DIMENSIONS
        sizes1 = [random.randint(2, MAX_SIDE_SIZE) for _ in xrange(DIMENSIONS)]
        b1 = Box(coords1, sizes1)
        
        coords2 = [random.randint(1, sizes1[i]-1) for  i in xrange(DIMENSIONS)]
        sizes2 = [random.randint(sizes1[i]-coords2[i]+1, max(sizes1[i]-coords2[i]+1, MAX_SIDE_SIZE)) for i in xrange(DIMENSIONS)]
        b2 = Box(coords2, sizes2)
        
        f1 = b1.toFormula()
        f2 = b2.toFormula()
    
        for key, models in MODELS.items():
            sd1 = f1.manyModelSatdelta(models)
            sd2 = f2.manyModelSatdelta(models)
            results3[key].append(abs(sd1 - sd2))

    print "case-3 (partial overlap):"
    for key, values in results3.items():
        print "  {} = {:.2f}".format(key, average(values))
    print


    # Case 4 (no overlap).
    results4 = dict([(key, list()) for key in MODELS])
    for i in xrange(NR_FORMULAS):
        coords1 = [0]*DIMENSIONS
        sizes1 = [random.randint(1, MAX_SIDE_SIZE) for _ in xrange(DIMENSIONS)]
        b1 = Box(coords1, sizes1)
        
        coords2 = [sizes1[i] + random.randint(1, MAX_SIDE_SIZE) for i in xrange(DIMENSIONS)]
        sizes2 = [random.randint(1, MAX_SIDE_SIZE) for _ in xrange(DIMENSIONS)]
        b2 = Box(coords2, sizes2)
        
        f1 = b1.toFormula()
        f2 = b2.toFormula()

        for key, models in MODELS.items():
            sd1 = f1.manyModelSatdelta(models)
            sd2 = f2.manyModelSatdelta(models)
            results4[key].append(abs(sd1 - sd2))

    print "case-4 (no overlap):"
    for key, values in results4.items():
        print "  {} = {:.2f}".format(key, average(values))