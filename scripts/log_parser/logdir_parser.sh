if [ $# -ne 2 ]
then
    echo "Usage: $0 log-file flags"
    exit 1
fi

find $1 -type f -name "*.log" | while read line
do
    python log_parser.py ${line} $2
done
