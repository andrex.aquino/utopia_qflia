import sys


def toSexpr(f):
    result = ""
    
    lines = f.split("\n")
    assert(len(lines) > 0)
    
    # Decode and drop the header.
    nr_clauses, nr_vars = map(int, lines.pop(0).split(" "))
    assert(len(lines) == nr_clauses)
    
    result = "{}\n".format(nr_clauses)
    
    for line in lines:
        tokens = line.split(" ")
        nr_ineqs = int(tokens.pop(0))
        
        result += str(nr_ineqs)
        
        for i in xrange(nr_ineqs):
            comp = tokens.pop(0)
            fc = int(tokens.pop(0))
            nr_terms = int(tokens.pop(0))
            assert(nr_terms > 0)
            
            result += " {}".format(comp)
            if (nr_terms > 1):
                result += " + {}".format(nr_terms)
            
            for j in xrange(nr_terms):
                nr_vars = int(tokens.pop(0))
                c = int(tokens.pop(0))
                
                result += " * {} c {}".format(nr_vars+1, c)
                
                for k in xrange(nr_vars):
                    v = tokens.pop(0)
                    assert("x" in v)
                    result += " v {}".format(v.replace("x", ""))

            result += " c {}\n".format(fc)

    return result


if __name__ == '__main__':
    pname, args = sys.argv[0], sys.argv[1:]

    if len(args) != 1:
        print "Usage: python {} tab_dataset_file".format(pname)
        exit(-1)

    fh = open(args[0], "r")
    formulas = fh.read().split("\n\n")
    fh.close()

    for block in formulas:
        f = block.strip()
        if f == "":
            continue
    
        sexpr = toSexpr(f)
        print sexpr