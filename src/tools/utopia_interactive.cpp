#include <boost/program_options.hpp>
#include <cstddef>
#include <fstream>
#include <iostream>
#include <memory> // includes `std::shared_ptr`.

#include "commons/timer/timer.h"
#include "core/formula/formula.h"
#include "core/formula/formula_factory.h"
#include "core/solver/utopia_solver/manager/solver_manager.h"


#define VERSION "1.0"


namespace po = boost::program_options;

std::string getCommandString(int argc, char *argv[]) {
    std::stringstream ss;
    for (int i=0; i<argc; ++i) {
        if (i > 0) {
            ss << " ";
        }
        ss << argv[i];
    }
    return ss.str();
}

void printUsage(char *pname, po::options_description &options) {
	std::cout << "Usage: " << pname << " [OPTIONS] ..." << std::endl;
	std::cout << options << std::endl;
}

void printUsageAndFail(std::string message, char *pname, po::options_description &options) {
	std::cerr << "Error: " << message << "." << std::endl;
	printUsage(pname, options);
	std::exit(EXIT_FAILURE);
}

int main(int argc, char *argv[]) {
	// Get the program name.
	char *pname = argv[0];
    
    // Reconstruct the full command use to launch this program.
    std::string command_string = getCommandString(argc, argv);

	// Get a reference to the cache manager.
	SolverManager &solver_manager = SolverManager::getInstance();

	// Extract a string listing all solvers.
	std::string valid_solvers = solver_manager.info();

	// Declare option help messages.
	std::string h_help = "print the help message";
	std::string h_version = "print the current version";
	std::string h_verbose = "print more data while solving formulas";
	std::string h_no_slicing = "disable the slicer component";
    std::string h_no_hash_cache = "disable the hash cache component";
	std::string h_no_sat_cache = "disable the sat cache component";
	std::string h_no_unsat_cache = "disable the unsat cache component";
	std::string h_tier1_candidates = "set the maximum number of candidates retrieved during the tier1 search";
	std::string h_tier2_candidates  = "set the maximum number of candidates retrieved during the tier2 search";
	std::string h_solver = "the specific instance of the solver to use. Valid values are: " + valid_solvers;
	std::string h_solver_timeout = "timeout for the solver in milliseconds, if set to zero no timeout is used";

	// Set the options of the parser dividing them in groups.
	po::options_description options("Options");
	options.add_options()
        ("help,h", h_help.c_str())
		("version", h_version.c_str())
		("verbose", h_verbose.c_str())
        ("no-slicing", h_no_slicing.c_str())
		("no-hash-cache", h_no_hash_cache.c_str())
		("no-sat-cache", h_no_sat_cache.c_str())
		("no-unsat-cache", h_no_unsat_cache.c_str())
		("tier1-candidates", po::value<std::size_t>()->default_value(50), h_tier1_candidates.c_str())
		("tier2-candidates", po::value<std::size_t>()->default_value(10), h_tier2_candidates.c_str())
		("solver", po::value<std::string>()->required(), h_solver.c_str())
		("solver-timeout", po::value<unsigned>()->default_value(0), h_solver_timeout.c_str());

	// Parse command line arguments and build the variables map.
	po::variables_map vm;
	try {
		po::store(po::command_line_parser(argc, argv).options(options).run(), vm);

		// Display help and exit.
		if (vm.count("help")) {
			printUsage(pname, options);
			std::exit(EXIT_SUCCESS);
		}

		// Display version and exit.
		if (vm.count("version")) {
			std::cout << "Version: " << VERSION << std::endl;
			std::exit(EXIT_SUCCESS);
		}

		po::notify(vm);
	} catch (po::error &e) {
		// On error, display the error and exit.
		printUsageAndFail(e.what(), pname, options);
	}

	// Extract all parameters.
	bool opt_verbose = vm.count("verbose");
	bool opt_slicing = !(vm.count("no-slicing"));
    bool opt_hash_cache = !(vm.count("no-hash-cache"));
	bool opt_sat_cache = !(vm.count("no-sat-cache"));
	bool opt_unsat_cache = !(vm.count("no-unsat-cache"));
	std::size_t opt_tier1_candidates = vm["tier1-candidates"].as<std::size_t>();
	std::size_t opt_tier2_candidates = vm["tier2-candidates"].as<std::size_t>();
	std::string opt_solver = vm["solver"].as<std::string>();
	unsigned opt_timeout = vm["solver-timeout"].as<unsigned>();

	// If the cache identifier is not valid, print an error and exit.
	if (!solver_manager.contains(opt_solver)) {
		printUsageAndFail("solver is not valid", pname, options);
	}

	// Get the solver from the solver manager.
	std::shared_ptr<UtopiaSolver> solver = solver_manager.get(opt_solver);

	// Configure the solver.
	solver->setTimeout(opt_timeout);

	solver->getSatUtopiaCache()->setTier1Candidates(opt_tier1_candidates);
	solver->getSatUtopiaCache()->setTier2Candidates(opt_tier2_candidates);
	solver->getUnsatUtopiaCache()->setTier1Candidates(opt_tier1_candidates);
	solver->getUnsatUtopiaCache()->setTier2Candidates(opt_tier2_candidates);

	if (opt_hash_cache) {
		solver->enableHashCache();
	} else {
		solver->disableHashCache();
	}

	if (opt_sat_cache) {
		solver->enableSatUtopiaCache();
	} else {
		solver->disableSatUtopiaCache();
	}

	if (opt_unsat_cache) {
		solver->enableUnsatUtopiaCache();
	} else {
		solver->disableUnsatUtopiaCache();
	}

	// Print configuration options.
    if (opt_verbose) {
        std::cout
        << "{"
        << "\"config\": {"
        << "\"command\": \"" << command_string << "\", "
        << "\"hash_cache\": " << opt_hash_cache << ", "
        << "\"sat_cache\": " << opt_sat_cache << ", "
        << "\"unsat_cache\": " << opt_unsat_cache << ", "
        << "\"tier1_candidates\": " << opt_tier1_candidates << ", "
        << "\"tier2_candidates\": " << opt_tier2_candidates << ", "
        << "\"solver\": \"" << opt_solver << "\", "
        << "\"timeout\": " << opt_timeout
        << "}"
        << "}"
        << std::endl;
    }

	// Solve formulas.
	std::size_t sat = 0;
	std::size_t unsat = 0;
	std::size_t unknown = 0;

    std::chrono::microseconds::rep decode_time = 0;
    std::chrono::microseconds::rep solver_time = 0;

    Timer timer;
	FormulaFactory formula_factory;
    
    std::string line;
    std::string formula = "";
    std::shared_ptr<Formula> f;

    if (opt_verbose) {
        std::cout
            << "Interactive mode."
            << std::endl << "Enter a formula in SEXPR format."
            << std::endl << "Type `check` on a single line to search it."
            << std::endl << "Type `exit` on a single line to exit the program."
            << std::endl;
    }

    while (std::cin) {
        getline(std::cin, line);
        if (line == "assert") {
            timer.start();
            std::stringstream formula_ss;
            formula_ss << formula;
            try {
                f = formula_factory.parseFormula(formula_ss);
                std::cout << "ok" << std::endl;
            } catch (ParsingError &e) {
                std::cout << "error" << std::endl;
                formula = "";
                f = nullptr;
            }
            decode_time += timer.micro();
        } else if (line == "check") {
            if (f != nullptr) {
                std::cout << "ok" << std::endl;
            } else {
                std::cout << "error" << std::endl;
                continue;
            }
            
            // Solve formula.
            timer.start();
            CheckResult status;
            if (opt_slicing) {
                status = solver->sliceAndCheck(f);
            } else {
                status = solver->check(f);
            }
            solver_time += timer.micro();

            switch (status) {
                case CheckResult::SAT:
                    ++sat;
                    std::cout << "sat" << std::endl;
                    break;
                case CheckResult::UNSAT:
                    ++unsat;
                    std::cout << "unsat" << std::endl;
                    break;
                case CheckResult::UNKNOWN:
                    ++unknown;
                    std::cout << "unknown" << std::endl;
                    break;
            }
            
            std::cout << solver->getStats() << std::endl;
            
            // Clear the formula.
            formula = "";
            f = nullptr;
        } else if (line == "exit") {
            std::exit(EXIT_SUCCESS);
        } else {
            formula += line + "\n";
        }
    }

	std::exit(EXIT_SUCCESS);
	return 0;
}
