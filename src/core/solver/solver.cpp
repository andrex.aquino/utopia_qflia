#include "solver.h"

#include <assert.h>


Solver::~Solver() {}

void Solver::setModel(std::shared_ptr<Model> model) {
	this->model = model;
}

void Solver::setUnsatCore(std::shared_ptr<UnsatCore> core) {
	this->core = core;
}

std::shared_ptr<Model> Solver::getModel() const {
    assert(this->model != nullptr);
	return this->model;
}

std::shared_ptr<UnsatCore> Solver::getUnsatCore() const {
	assert(this->core != nullptr);
    return this->core;
}

CheckResult Solver::sliceAndCheck(std::shared_ptr<Formula> formula) {
    std::vector<std::shared_ptr<Formula> > subformulas;
    formula->slice(subformulas);
    
    std::size_t unknown_subformulas = 0;
    for (auto it=subformulas.begin(); it!=subformulas.end(); ++it) {
        CheckResult status = this->check(*it);
        if (status == CheckResult::UNSAT) {
            // TODO -- reconstruct and store unsat-core.
            return CheckResult::UNSAT;
        } else if (status == CheckResult::UNKNOWN) {
            ++unknown_subformulas;
        }
    }
    
    if (unknown_subformulas > 0) {
        return CheckResult::UNKNOWN;
    }
    
    // TODO -- reconstruct and store model.
    return CheckResult::SAT;
}
