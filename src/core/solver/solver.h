#ifndef CORE_SOLVER_SOLVER_H
#define CORE_SOLVER_SOLVER_H

#include <memory>

#include "core/formula/formula.h"


enum CheckResult {
	SAT,
	UNSAT,
	UNKNOWN
};


class Solver {
	protected:
		std::shared_ptr<Model> model;
		std::shared_ptr<UnsatCore> core;

		// Updates the model stored by this solver.
		void setModel(std::shared_ptr<Model> model);

		// Updates the unsat-core stored by this solver.
		void setUnsatCore(std::shared_ptr<UnsatCore> core);

	public:
		virtual ~Solver();

		// Returns the model stored during the last call to check.
		std::shared_ptr<Model> getModel() const;

		// Returns the unsat-core stored during the last call to check.
		std::shared_ptr<UnsatCore> getUnsatCore() const;
    
        // Returns the satisfiability value of `formula` by solving
        // its independent subformulas.
        CheckResult sliceAndCheck(std::shared_ptr<Formula> formula);

		// Returns the satisfiability value of `formula`.
		virtual CheckResult check(std::shared_ptr<Formula> formula) = 0;
};

#endif
