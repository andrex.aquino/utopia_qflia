#include "utopia_solver.h"

#include <sstream>


UtopiaSolver::UtopiaSolver(
    std::shared_ptr<UtopiaCache<Model> > sat_utopia_cache,
    std::shared_ptr<UtopiaCache<UnsatCore> > unsat_utopia_cache) :
        sat_utopia_cache(sat_utopia_cache),
        unsat_utopia_cache(unsat_utopia_cache),
        use_hash_cache(true),
        use_sat_utopia_cache(true),
        use_unsat_utopia_cache(true),
        checks(0),
        hash_cache_hits(0),
        sat_utopia_cache_hits(0),
        unsat_utopia_cache_hits(0),
        solver_calls(0) {};

UtopiaSolver::~UtopiaSolver() {};

const std::map<unsigned long, std::shared_ptr<Model> >& UtopiaSolver::getSatHashCache() const {
	return this->sat_hash_cache;
};

const std::map<unsigned long, std::shared_ptr<UnsatCore> >& UtopiaSolver::getUnsatHashCache() const {
	return this->unsat_hash_cache;
};

std::shared_ptr<UtopiaCache<Model> > UtopiaSolver::getSatUtopiaCache() const {
	return this->sat_utopia_cache;
};

std::shared_ptr<UtopiaCache<UnsatCore> > UtopiaSolver::getUnsatUtopiaCache() const {
	return this->unsat_utopia_cache;
}

void UtopiaSolver::enableHashCache() {
	this->use_hash_cache = true;
}

void UtopiaSolver::disableHashCache() {
	this->use_hash_cache = false;
}

void UtopiaSolver::enableSatUtopiaCache() {
	this->use_sat_utopia_cache = true;
}

void UtopiaSolver::disableSatUtopiaCache() {
	this->use_sat_utopia_cache = false;
}

void UtopiaSolver::enableUnsatUtopiaCache() {
	this->use_unsat_utopia_cache = true;
}

void UtopiaSolver::disableUnsatUtopiaCache() {
	this->use_unsat_utopia_cache = false;
}

std::string UtopiaSolver::getStats() const {
    std::stringstream result;
    result
        << "{"
        << "\"checks\": " << this->checks << ", "
        << "\"hash_cache_hits\": " << this->hash_cache_hits << ", "
        << "\"sat_utopia_cache_hits\": " << this->sat_utopia_cache_hits << ", "
        << "\"unsat_utopia_cache_hits\": " << this->unsat_utopia_cache_hits << ", "
        << "\"solver_calls\": " << this->solver_calls << ", "
        << "\"sat_hash_cache_size\": " << this->sat_hash_cache.size() << ", "
        << "\"unsat_hash_cache_size\": " << this->unsat_hash_cache.size() << ", "
        << "\"sat_utopia_cache_size\": " << this->sat_utopia_cache->getEntries().size() << ", "
        << "\"unsat_utopia_cache_size\": " << this->unsat_utopia_cache->getEntries().size()
        << "}";
    return result.str();
}

CheckResult UtopiaSolver::check(std::shared_ptr<Formula> formula) {
    ++this->checks;
	unsigned long formula_hash = formula->getHash();

	if (this->use_hash_cache) {
        // Check the sat hash cache first.
		auto shc_it = this->sat_hash_cache.find(formula_hash);
		if (shc_it != this->sat_hash_cache.end()) {
            ++this->hash_cache_hits;
            this->setModel(shc_it->second);
            this->setUnsatCore(nullptr);
			return CheckResult::SAT;
		}
        
        // Check the unsat hash cache.
		auto uhc_it = this->unsat_hash_cache.find(formula_hash);
		if (uhc_it != this->unsat_hash_cache.end()) {
            ++this->hash_cache_hits;
            this->setModel(nullptr);
            this->setUnsatCore(uhc_it->second);
			return CheckResult::UNSAT;
		}
	}

	if (this->use_sat_utopia_cache) {
        // Check the sat utopia cache.
		if (this->sat_utopia_cache->search(formula)) {
            ++this->sat_utopia_cache_hits;
			this->setModel(this->sat_utopia_cache->getSolution());
            this->setUnsatCore(nullptr);
            this->sat_hash_cache[formula_hash] = this->sat_utopia_cache->getSolution(); // update sat hash cache.
			return CheckResult::SAT;
		}
	}

	if (this->use_unsat_utopia_cache) {
        // Check the unsat utopia cache.
		if (this->unsat_utopia_cache->search(formula)) {
            ++this->unsat_utopia_cache_hits;
            this->setModel(nullptr);
			this->setUnsatCore(this->unsat_utopia_cache->getSolution());
            this->unsat_hash_cache[formula_hash] = this->unsat_utopia_cache->getSolution(); // update unsat hash cache.
			return CheckResult::UNSAT;
		}
	}

	// Solve the formula and set model or unsat-core accordingly.
	CheckResult status = Z3Solver::check(formula);
    ++this->solver_calls;

	// If this solver is configured to store formulas on cache
	// misses save the current formula in the right cache.
    switch (status) {
        case CheckResult::SAT:
            this->sat_utopia_cache->store(formula, this->getModel());
            this->sat_hash_cache[formula_hash] = this->getModel();
            break;
        case CheckResult::UNSAT:
            this->unsat_utopia_cache->store(formula, this->getUnsatCore());
            this->unsat_hash_cache[formula_hash] = this->getUnsatCore();
            break;
        case CheckResult::UNKNOWN:
            // Do nothing.
            break;
    }

	return status;
}

CheckResult UtopiaSolver::sliceAndCheck(std::shared_ptr<Formula> formula) {
    // Create a backup of the cache statistics.
    std::size_t bck_checks = this->checks;
    std::size_t bck_hash_cache_hits = this->hash_cache_hits;
    std::size_t bck_sat_utopia_cache_hits = this->sat_utopia_cache_hits;
    std::size_t bck_unsat_utopia_cache_hits = this->unsat_utopia_cache_hits;
    std::size_t bck_solver_calls = this->solver_calls;
    
    // Call the slice and check procedure.
    CheckResult status = Z3Solver::sliceAndCheck(formula);
    
    // Get the difference in the cache statistics after the check.
    std::size_t delta_checks = this->checks - bck_checks;
    std::size_t delta_hash_cache_hits = this->hash_cache_hits - bck_hash_cache_hits;
    std::size_t delta_sat_utopia_cache_hits = this->sat_utopia_cache_hits - bck_sat_utopia_cache_hits;
    std::size_t delta_unsat_utopia_cache_hits = this->unsat_utopia_cache_hits - bck_unsat_utopia_cache_hits;
    std::size_t delta_solver_calls = this->solver_calls - bck_solver_calls;
    
    // Restore backup statistics.
    this->checks = bck_checks;
    this->hash_cache_hits = bck_hash_cache_hits;
    this->sat_utopia_cache_hits = bck_sat_utopia_cache_hits;
    this->unsat_utopia_cache_hits = bck_unsat_utopia_cache_hits;
    this->solver_calls = bck_solver_calls;
    
    // Update statistics.
    ++this->checks;
    
    if (delta_solver_calls > 0) {
        ++this->solver_calls;
    } else if (status == CheckResult::SAT) {
        if (delta_sat_utopia_cache_hits > 0) {
            ++this->sat_utopia_cache_hits;
        } else {
            ++this->hash_cache_hits;
        }
    } else if (status == CheckResult::UNSAT) {
        if (delta_unsat_utopia_cache_hits > 0) {
            ++this->unsat_utopia_cache_hits;
        } else {
            ++this->hash_cache_hits;
        }
    }
    
    return status;
}
