#ifndef CORE_SOLVER_MANAGER_SOLVER_MANAGER_H
#define CORE_SOLVER_MANAGER_SOLVER_MANAGER_H

#include <memory>
#include <string>

#include "core/solver/utopia_solver/utopia_solver.h"


// A singleton class exporting registered solvers.
class SolverManager {
  private:
    std::map<std::string, std::shared_ptr<UtopiaSolver> > name_to_solver;

    SolverManager();
    virtual ~SolverManager();

    SolverManager(SolverManager const&) = delete;
    void operator=(SolverManager const&) = delete;

  public:
    // Returns a reference to the cache manager.
    static SolverManager& getInstance();

    // Returns a string listing the available solvers.
    std::string info() const;

    // Returns the cache associated with the given name if it exists.
    // It returns `nullptr` otherwise.
    std::shared_ptr<UtopiaSolver> get(std::string &name) const;

    // Returns `true` if the cache associated with the given name exists.
    bool contains(std::string &name) const;
};

#endif
