#include "core/solver/utopia_solver/manager/solver_manager.h"

#include "core/cache/utopia_cache/const_cache/const_cache.h"
#include "core/cache/utopia_cache/hamming_cache/hamming_cache.h"
#include "core/cache/utopia_cache/jaccard_cache/jaccard_cache.h"
#include "core/cache/utopia_cache/random_cache/random_cache.h"
#include "core/cache/utopia_cache/satclauses_cache/satclauses_cache.h"
#include "core/cache/utopia_cache/satdelta_cache/satdelta_cache.h"
#include "core/cache/utopia_cache/ted_cache/ted_cache.h"

#define CREATE_UTOPIA_SOLVER(cache) (\
    std::shared_ptr<UtopiaSolver>(\
        new UtopiaSolver(\
            std::shared_ptr<cache<Model> >(new cache<Model>()),\
            std::shared_ptr<cache<UnsatCore> >(new cache<UnsatCore>())\
        )\
    )\
)


typedef std::pair<std::string, std::shared_ptr<UtopiaSolver> > named_solver;


SolverManager::SolverManager() {
    this->name_to_solver.insert(named_solver("k0", CREATE_UTOPIA_SOLVER(ConstCache)));
    this->name_to_solver.insert(named_solver("hamming", CREATE_UTOPIA_SOLVER(HammingCache)));
	this->name_to_solver.insert(named_solver("jaccard", CREATE_UTOPIA_SOLVER(JaccardCache)));
    this->name_to_solver.insert(named_solver("random", CREATE_UTOPIA_SOLVER(RandomCache)));
    this->name_to_solver.insert(named_solver("satclauses", CREATE_UTOPIA_SOLVER(SatClausesCacheA3)));
    this->name_to_solver.insert(named_solver("satclauses_A1", CREATE_UTOPIA_SOLVER(SatClausesCacheA1)));
    this->name_to_solver.insert(named_solver("satclauses_B1", CREATE_UTOPIA_SOLVER(SatClausesCacheB1)));
    this->name_to_solver.insert(named_solver("satclauses_C1", CREATE_UTOPIA_SOLVER(SatClausesCacheC1)));
    this->name_to_solver.insert(named_solver("satclauses_D1", CREATE_UTOPIA_SOLVER(SatClausesCacheD1)));
    this->name_to_solver.insert(named_solver("satclauses_E1", CREATE_UTOPIA_SOLVER(SatClausesCacheE1)));
    this->name_to_solver.insert(named_solver("satclauses_A3", CREATE_UTOPIA_SOLVER(SatClausesCacheA3)));
    this->name_to_solver.insert(named_solver("satclauses_B3", CREATE_UTOPIA_SOLVER(SatClausesCacheB3)));
    this->name_to_solver.insert(named_solver("satclauses_C3", CREATE_UTOPIA_SOLVER(SatClausesCacheC3)));
    this->name_to_solver.insert(named_solver("satclauses_A5", CREATE_UTOPIA_SOLVER(SatClausesCacheA5)));
    this->name_to_solver.insert(named_solver("satclauses_B5", CREATE_UTOPIA_SOLVER(SatClausesCacheB5)));
    this->name_to_solver.insert(named_solver("satclauses_A9", CREATE_UTOPIA_SOLVER(SatClausesCacheA9)));
    this->name_to_solver.insert(named_solver("satclauses_B9", CREATE_UTOPIA_SOLVER(SatClausesCacheB9)));
    this->name_to_solver.insert(named_solver("satdelta", CREATE_UTOPIA_SOLVER(SatDeltaCacheA3)));
    this->name_to_solver.insert(named_solver("satdelta_A1", CREATE_UTOPIA_SOLVER(SatDeltaCacheA1)));
    this->name_to_solver.insert(named_solver("satdelta_B1", CREATE_UTOPIA_SOLVER(SatDeltaCacheB1)));
    this->name_to_solver.insert(named_solver("satdelta_C1", CREATE_UTOPIA_SOLVER(SatDeltaCacheC1)));
    this->name_to_solver.insert(named_solver("satdelta_D1", CREATE_UTOPIA_SOLVER(SatDeltaCacheD1)));
    this->name_to_solver.insert(named_solver("satdelta_E1", CREATE_UTOPIA_SOLVER(SatDeltaCacheE1)));
    this->name_to_solver.insert(named_solver("satdelta_A3", CREATE_UTOPIA_SOLVER(SatDeltaCacheA3)));
    this->name_to_solver.insert(named_solver("satdelta_B3", CREATE_UTOPIA_SOLVER(SatDeltaCacheB3)));
    this->name_to_solver.insert(named_solver("satdelta_C3", CREATE_UTOPIA_SOLVER(SatDeltaCacheC3)));
    this->name_to_solver.insert(named_solver("satdelta_A5", CREATE_UTOPIA_SOLVER(SatDeltaCacheA5)));
    this->name_to_solver.insert(named_solver("satdelta_B5", CREATE_UTOPIA_SOLVER(SatDeltaCacheB5)));
    this->name_to_solver.insert(named_solver("satdelta_A9", CREATE_UTOPIA_SOLVER(SatDeltaCacheA9)));
    this->name_to_solver.insert(named_solver("satdelta_B9", CREATE_UTOPIA_SOLVER(SatDeltaCacheB9)));
    this->name_to_solver.insert(named_solver("ted", CREATE_UTOPIA_SOLVER(TedCache)));
};

SolverManager::~SolverManager() {};

SolverManager& SolverManager::getInstance() {
	static SolverManager manager;
	return manager;
}

std::string SolverManager::info() const {
	std::string result = "";
	int i = 0;
	for (auto it=this->name_to_solver.begin(); it!=this->name_to_solver.end(); ++it, ++i) {
		if (i > 0) {
			result += ", ";
		}
		result += it->first;
	}
	return result;
}

std::shared_ptr<UtopiaSolver> SolverManager::get(std::string &name) const {
	auto it = this->name_to_solver.find(name);
	if (it != this->name_to_solver.end()) {
		return it->second;
	}
	return nullptr;
}

bool SolverManager::contains(std::string &name) const {
	std::shared_ptr<UtopiaSolver> cache;
	cache = this->get(name);
	if (cache == nullptr) {
		return false;
	}
	return true;
}
