#ifndef CORE_SOLVER_UTOPIA_SOLVER_UTOPIA_SOLVER_H
#define CORE_SOLVER_UTOPIA_SOLVER_UTOPIA_SOLVER_H

#include <cstddef>
#include <map>
#include <memory>
#include <string>

#include "core/cache/utopia_cache/utopia_cache.h"
#include "core/solver/z3_solver/z3_solver.h"


class UtopiaSolver: public Z3Solver {
	private:
		std::map<unsigned long, std::shared_ptr<Model> > sat_hash_cache;
        std::map<unsigned long, std::shared_ptr<UnsatCore> > unsat_hash_cache;
		std::shared_ptr<UtopiaCache<Model> > sat_utopia_cache;
		std::shared_ptr<UtopiaCache<UnsatCore> > unsat_utopia_cache;

		// Options.
		bool use_hash_cache;
		bool use_sat_utopia_cache;
		bool use_unsat_utopia_cache;
    
        // Counters.
        std::size_t checks;
        std::size_t hash_cache_hits;
        std::size_t sat_utopia_cache_hits;
        std::size_t unsat_utopia_cache_hits;
        std::size_t solver_calls;

	public:
		UtopiaSolver(
            std::shared_ptr<UtopiaCache<Model> > sat_utopia_cache,
            std::shared_ptr<UtopiaCache<UnsatCore> > unsat_utopia_cache);

		virtual ~UtopiaSolver();

		// Returns a reference to the sat hash cache component of this cache.
		const std::map<unsigned long, std::shared_ptr<Model> >& getSatHashCache() const;
    
        // Returns a reference to the unsat hash cache component of this cache.
		const std::map<unsigned long, std::shared_ptr<UnsatCore> >& getUnsatHashCache() const;

		// Returns a pointer to the sat utopia cache component of this cache.
		std::shared_ptr<UtopiaCache<Model> > getSatUtopiaCache() const;

		// Returns a pointer to the unsat utopia cache component of this cache.
		std::shared_ptr<UtopiaCache<UnsatCore> > getUnsatUtopiaCache() const;

		// Enables the hash cache.
		void enableHashCache();

		// Disables the hash cache.
		void disableHashCache();

		// Enables the sat cache.
		void enableSatUtopiaCache();

		// Disables the sat cache.
		void disableSatUtopiaCache();

		// Enables the unsat cache.
		void enableUnsatUtopiaCache();

		// Disables the unsat cache.
		void disableUnsatUtopiaCache();
    
        // Returns a JSON string representing the current state of the cache.
        std::string getStats() const;

		// Returns a pointer to a solution proving that this formula is satisfiable
		// or unsatisfiable. If no solution can be found returns `nullptr`.
		CheckResult check(std::shared_ptr<Formula> formula);
    
        // Returns a pointer to a solution proving that this formula is satisfiable
		// or unsatisfiable. If no solution can be found returns `nullptr`.
		CheckResult sliceAndCheck(std::shared_ptr<Formula> formula);
};

#endif
