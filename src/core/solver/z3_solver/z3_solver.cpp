#include "z3_solver.h"


Z3Solver::Z3Solver() : timeout(0) {}

Z3Solver::~Z3Solver() {}

unsigned Z3Solver::getTimeout() const {
	return this->timeout;
}

void Z3Solver::setTimeout(unsigned timeout) {
	this->timeout = timeout;
}

z3::expr Z3Solver::clauseToZ3Expr(std::shared_ptr<Clause> clause, z3::context &c) const {
	// Encode the clause in SMT-LIB v2 format.
	std::string z3_assert = "";
	const std::set<std::size_t> &clause_vars = clause->getVars();
	
	for (auto it=clause_vars.begin(); it!=clause_vars.end(); ++it) {
		z3_assert += "(declare-fun x" + std::to_string(*it) + " () Int)";
	}
	z3_assert += "(assert " + clause->toSmt() + ")";

	// Transform it into a Z3 expression.
	Z3_ast ast = Z3_parse_smtlib2_string(
			(_Z3_context*)c,
			z3_assert.c_str(),
			0, 0, 0, 0, 0, 0
	);
	z3::expr expr(c, ast);
	return expr;
}

std::shared_ptr<Model> Z3Solver::decodeZ3Model(z3::model &model) const {
	std::map<std::size_t, mpz_class> map;
	for (unsigned i=0; i<model.size(); ++i) {
		z3::func_decl func = model[i];
		z3::symbol z3_var = func.name();
		z3::expr z3_value(model.get_const_interp(func));

		std::string var = z3_var.str();
		if (var[0] == 'x') { // Ignore variables representing clauses.
			var.erase(var.begin()); // Drop 'x'.
			std::size_t var_index = std::stoi(var);

			Z3_string s_value = Z3_get_numeral_string(
					(_Z3_context*)z3_value.ctx(),
					(_Z3_ast*)z3_value
			);
			mpz_class value;
			value.set_str(s_value, 10);

			map.insert(std::pair<std::size_t, mpz_class>(var_index, value));
		}
	}
	return std::shared_ptr<Model>(new Model(map));
}

std::shared_ptr<UnsatCore> Z3Solver::decodeZ3UnsatCore(
		z3::expr_vector &core,
		std::map<std::string, unsigned long> &id_to_clause_map) const {

	std::vector<unsigned long> clauses;
	for (std::size_t i=0; i<core.size(); ++i) {
		z3::expr c = core[i];
		std::string clause_id = c.decl().name().str();

		unsigned long clause_hash = id_to_clause_map[clause_id];
		clauses.push_back(clause_hash);
	}
	return std::shared_ptr<UnsatCore>(new UnsatCore(clauses));
}

CheckResult Z3Solver::check(std::shared_ptr<Formula> formula) {
	// Instantiate the Z3 solver.
	z3::context c;
	z3::solver solver(c, "QF_LIA");

	// Configure the solver.
	z3::params params(c);
	params.set(":unsat_core", true);
	if (this->timeout > 0) {
		params.set(":timeout", static_cast<unsigned>(this->getTimeout()));
	}
	solver.set(params);

	// Assert all clauses naming them progressively to track unsat-cores.
	std::map<std::string, unsigned long> id_to_clause_map;
	std::size_t i = 0;
	for (auto it=formula->getClauses().begin(); it!=formula->getClauses().end(); ++it, ++i) {
		z3::expr clause_expr(this->clauseToZ3Expr(*it, c));
		std::string clause_id = "c" + std::to_string(i);
		solver.add(clause_expr, clause_id.c_str());
		id_to_clause_map[clause_id] = (*it)->getHash();
	}

	// Check the satisfiability of this formula.
	z3::check_result status = solver.check();

	switch (status) {
		case z3::sat: {
			z3::model model = solver.get_model();
			this->setModel(this->decodeZ3Model(model));
			this->setUnsatCore(nullptr);
			return CheckResult::SAT;
			break;
		}
		case z3::unsat: {
			z3::expr_vector core = solver.unsat_core();
			this->setModel(nullptr);
			this->setUnsatCore(this->decodeZ3UnsatCore(core, id_to_clause_map));
			return CheckResult::UNSAT;
			break;
		}
		case z3::unknown: {
			return CheckResult::UNKNOWN;
			this->setModel(nullptr);
			this->setUnsatCore(nullptr);
			return CheckResult::UNKNOWN;
			break;
		}
	}

	return CheckResult::UNKNOWN;
}
