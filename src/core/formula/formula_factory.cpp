#include "formula_factory.h"

#include <cstddef>


// Parsing error.

ParsingError::ParsingError(std::string message) : message(message) {}

ParsingError::~ParsingError() throw() {}

const char* ParsingError::what() const throw() {
    return message.c_str();
}


// Formula factory.

FormulaFactory::FormulaFactory() {}

FormulaFactory::~FormulaFactory() {}

// Utility function to parse an expression in SEXPR format.
std::shared_ptr<Expr> FormulaFactory::parseExpression(std::istream &input) {
    std::string token;
    if (!(input >> token)) {
        throw ParsingError("Expression token is missing");
    }

    if (token == "c") { // Constant.
        mpz_class value;
        if (!(input >> value)) {
            throw ParsingError("Value for constant is missing");
        }
        return std::shared_ptr<Expr>(new Const(value));
    } else if (token == "v") { // Variable.
        std::size_t index;
        if (!(input >> index)) {
            throw ParsingError("Index for variable is missing");
        }
        return std::shared_ptr<Expr>(new Var(index));
    } else if (token == "+") { // Add.
        std::size_t nr_operands;
        if (!(input >> nr_operands)) {
            throw ParsingError("Number of operands of addition is missing");
        }
        std::shared_ptr<Expr> add(new Const(0));
        for (std::size_t i=0; i<nr_operands; ++i) {
            try {
                add = std::shared_ptr<Add>(
                    new Add(
                        add,
                        this->parseExpression(input)
                    )
                );
            } catch (ParsingError &e) {
                throw ParsingError("An operand of an addition cannot be decoded");
            }
        }
        return add;
    } else if (token == "*") { // Mul.
        std::size_t nr_operands;
        if (!(input >> nr_operands) || nr_operands != 2) {
            throw ParsingError("Number of operands of product is missing or is not 2");
        }

        std::shared_ptr<Expr> c;
        std::shared_ptr<Expr> expr;
        try {
            c = this->parseExpression(input);
            expr = this->parseExpression(input);
        } catch (ParsingError &e) {
            throw ParsingError("An operand of multiplication cannot be decoded");
        }

        std::shared_ptr<Const> c_const = std::dynamic_pointer_cast<Const>(c);
        if (c_const == nullptr) {
            throw ParsingError("First operand of product is not a constant");
        }

        return std::shared_ptr<Mul>(new Mul(c_const, expr));
    }

    // Unrecognized operation.
    throw ParsingError("Unrecognized expression token");
}

std::shared_ptr<Formula> FormulaFactory::parseFormula(std::istream &input) {
    std::vector<std::shared_ptr<Clause> > clauses;

    // Get the number of clauses to extract.
    std::size_t nr_clauses;
    if (!(input >> nr_clauses)) {
        throw ParsingError("Number of clauses is missing");
    }

    // Extract clauses.
    for (std::size_t i=0; i<nr_clauses; ++i) {
        std::vector<std::shared_ptr<Inequality> > ineqs;

        std::size_t clause_ineqs;
        if (!(input >> clause_ineqs)) {
            throw ParsingError("Number of inequalities in clause " + std::to_string(i) + " is missing");
        }

        for (std::size_t j=0; j<clause_ineqs; ++j) {
            // Extract the comparison of the current inequality.
            std::string comparison;
            if (!(input >> comparison)) {
                throw ParsingError(
                    "Comparison in clause " + std::to_string(i)
                    + " and inequality " + std::to_string(j)
                    + " is missing"
                );
            }

            // Build the inequality and add it to the current clause.
            Comparison comp = Comparison::LT;
            if (comparison == "lt") {
                comp = Comparison::LT;
            } else if (comparison == "le") {
                comp = Comparison::LE;
            } else if (comparison == "gt") {
                comp = Comparison::GT;
            } else if (comparison == "ge") {
                comp = Comparison::GE;
            } else if (comparison == "eq") {
                comp = Comparison::EQ;
            } else if (comparison == "ne") {
                comp = Comparison::NE;
            } else {
                throw ParsingError("Unrecognized comparison");
            }

            // Extract the left and right hand side of the inequality.
            std::shared_ptr<Expr> lhs;
            std::shared_ptr<Expr> rhs;
            try {
                lhs = this->parseExpression(input);
                rhs = this->parseExpression(input);
            } catch (ParsingError &e) {
                throw ParsingError(
                    "Cannot decode expression in clause " + std::to_string(i)
                    + " and inequality " + std::to_string(j)
                );
            }

            std::shared_ptr<Inequality> ineq(
                new Inequality(
                    lhs,
                    rhs,
                    comp
                )
            );
            ineqs.push_back(ineq);
        }
        clauses.push_back(std::shared_ptr<Clause>(new Clause(ineqs)));
    }
    return std::shared_ptr<Formula>(new Formula(clauses));
}
