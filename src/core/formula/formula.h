#ifndef CORE_FORMULA_FORMULA_H_
#define CORE_FORMULA_FORMULA_H_

#include <cstddef>
#include <gmpxx.h>
#include <istream>
#include <ostream>
#include <map>
#include <memory> // includes `std::shared_ptr`.
#include <set>
#include <string>
#include <vector>


typedef std::map<std::size_t, std::size_t> subst_map;


// Forward declaration of the FormulaVisitor class.
class FormulaVisitor;

// Forward declaration of the Formula class.
class Formula;


// Abstract class representing a solution of a formula.
// A solution is either a model or an unsat-core.
class Solution {
public:
    virtual ~Solution();

    // Returns the size of the solution.
    virtual std::size_t size() const = 0;

    // Returns true if this solution belongs to the solution space of `formula`.
    virtual bool check(std::shared_ptr<Formula> formula) = 0;

    // Returns a string representing this solution.
    virtual std::string toString() const = 0;
};


// An immutable model for a formula.
// Represented as a variable name to value map.
class Model: public Solution, public std::enable_shared_from_this<Model> {
private:
    std::map<std::size_t, mpz_class> map;

public:
    Model(std::map<std::size_t, mpz_class> &map);

    virtual ~Model();

    // Returns the number of assignments in this model.
    std::size_t size() const;

    // Returns the value associated to the given index in this model.
    // If no value is associated to the index, 0 is returned.
    mpz_class get(std::size_t index) const;

    // Returns `true` if this model satisfies `formula`.
    bool check(std::shared_ptr<Formula> formula);

    // Returns a string representing this model.
    std::string toString() const;
};


// An immutable unsat-core for a formula. Represented as a list of
// the hashes of the clauses that prove the formula unsatisfiable.
class UnsatCore: public Solution, public std::enable_shared_from_this<UnsatCore> {
private:
    std::vector<std::size_t> clauses;

public:
    UnsatCore(std::vector<std::size_t> &clauses);
    virtual ~UnsatCore();

    // Returns the number of clauses in this unsat-core.
    std::size_t size() const;

    // Returns the list of the hashes of the clauses in this unsat-core.
    const std::vector<std::size_t>& getClauses() const;

    // Returns `true` if this unsat-core is contained in `formula`.
    bool check(std::shared_ptr<Formula> formula);

    // Returns a string representing this unsat-core.
    std::string toString() const;
};


// An exception that may be thrown by some operations on formulas.
class OperationError: public std::exception {
public:
    std::string message;

    OperationError(std::string msg);
    virtual ~OperationError() throw();

    const char* what() const throw();
};


// Abstract class representing an expression.
class Expr {
protected:
    std::set<std::size_t> vars;
    std::size_t size;
    std::size_t hash;

public:
    virtual ~Expr() {};
    
    // Returns the number of nodes in the ast representing this expression.
    std::size_t getSize() const {
        return this->size;
    }

    // Returns the hash of this expression.
    std::size_t getHash() const {
        return this->hash;
    };

    virtual const std::set<std::size_t>& getVars() const = 0;
    virtual mpz_class subs(std::shared_ptr<Model> model) const = 0;
    virtual std::shared_ptr<Expr> renameVars(subst_map &subst) const = 0;
    virtual std::string toSmt() const = 0;
    virtual std::string toSexpr() const = 0;
    virtual std::shared_ptr<Expr> copy() const = 0;
    virtual void visit(FormulaVisitor &visitor) const = 0;
};


// An integer constant.
class Const: public Expr {
private:
    mpz_class value;

public:
    Const(mpz_class value);

    virtual ~Const();

    // Returns the value of this constant.
    mpz_class getValue() const;

    // Returns the empty set.
    const std::set<std::size_t>& getVars() const;

    // Returns the value of this constant.
    mpz_class subs(std::shared_ptr<Model> model) const;

    // Returns a copy of this constant.
    std::shared_ptr<Expr> renameVars(subst_map &subst) const;

    // Returns the SMT-LIB v2 representation of this constant.
    std::string toSmt() const;

    // Returns the SEXPR representation of this constant.
    std::string toSexpr() const;

    // Returns a copy of this constant.
    std::shared_ptr<Expr> copy() const;

    // Calls the method `onConst` of `visitor` passing this constant to it.
    void visit(FormulaVisitor &visitor) const;
};


// A variable.
class Var: public Expr {
private:
    std::size_t index;

public:
    Var(std::size_t index);

    virtual ~Var();

    // Returns the index of this variable.
    std::size_t getIndex() const;

    // Returns a set containing the index of this variable.
    const std::set<std::size_t>& getVars() const;

    // Returns the value associated to this variable in the model.
    mpz_class subs(std::shared_ptr<Model> model) const;

    // Renames this var according to the given substitution map.
    // A renamed copy of this variable is returned.
    //
    // Throws an OperationError exception if the substitution
    // map does not contain a mapping for this variable.
    std::shared_ptr<Expr> renameVars(subst_map &subst) const;

    // Returns the SMT-LIB v2 representation of this variable.
    std::string toSmt() const;

    // Returns the SEXPR representation of this variable.
    std::string toSexpr() const;

    // Returns a copy of this variable.
    std::shared_ptr<Expr> copy() const;

    // Calls the method `onVar` of `visitor` passing this variable to it.
    void visit(FormulaVisitor &visitor) const;
};


// The sum of two expressions.
class Add: public Expr {
private:
    std::shared_ptr<Expr> lhs;
    std::shared_ptr<Expr> rhs;

public:
    Add(std::shared_ptr<Expr> lhs, std::shared_ptr<Expr> rhs);

    virtual ~Add();

    std::shared_ptr<Expr> getLhs() const;
    std::shared_ptr<Expr> getRhs() const;

    // Returns the set of variables of this expression.
    const std::set<std::size_t>& getVars() const;

    // Evaluates this sum w.r.t. the given model.
    mpz_class subs(std::shared_ptr<Model> model) const;

    // Renames all variables in this operation according to the given
    // substitution map. This instance is not altered, a copy is returned.
    //
    // Throws an OperationError exception if this operation contains a
    // variable for which no substitution exists in the substitution map.
    std::shared_ptr<Expr> renameVars(subst_map &subst) const;

    // Returns the SMT-LIB v2 representation of this sum.
    std::string toSmt() const;

    // Returns the SEXPR representation of this sum.
    std::string toSexpr() const;

    // Returns a copy of this operation.
    std::shared_ptr<Expr> copy() const;

    // Calls the method `onAdd` of `visitor` passing this sum to it.
    void visit(FormulaVisitor &visitor) const;
};


// The product of a constant and an expression.
class Mul: public Expr {
private:
    std::shared_ptr<Const> c;
    std::shared_ptr<Expr> expr;

public:
    Mul(std::shared_ptr<Const> c, std::shared_ptr<Expr> expr);

    virtual ~Mul();

    std::shared_ptr<Const> getConst() const;
    std::shared_ptr<Expr> getExpr() const;

    // Returns the set of variables of this expression.
    const std::set<std::size_t>& getVars() const;

    // Evaluates this product w.r.t. the given model.
    mpz_class subs(std::shared_ptr<Model> model) const;

    // Renames all variables in this operation according to the given
    // substitution map. This instance is not altered, a copy is returned.
    //
    // Throws an OperationError exception if this operation contains a
    // variable for which no substitution exists in the substitution map.
    std::shared_ptr<Expr> renameVars(subst_map &subst) const;

    // Returns the SMT-LIB v2 representation of this product.
    std::string toSmt() const;

    // Returns the SEXPR representation of this product.
    std::string toSexpr() const;

    // Returns a copy of this operation.
    std::shared_ptr<Expr> copy() const;

    // Calls the method `onMul` of `visitor` passing this product to it.
    void visit(FormulaVisitor &visitor) const;
};


// Comparison signs for an inequality.
enum Comparison {
    LT,
    LE,
    GT,
    GE,
    EQ,
    NE
};


// An Inequality is a relation between two expressions.
//
// Valid relations are:
// 1. less than,
// 2. less than or equal,
// 3. greater than,
// 4. greater than or equal,
// 5. equal,
// 6. not equal.
class Inequality {
private:
    std::shared_ptr<Expr> lhs;
    std::shared_ptr<Expr> rhs;
    Comparison comp;
    
    std::set<std::size_t> vars;
    std::size_t size;
    std::size_t hash;

public:
    Inequality(
        std::shared_ptr<Expr> lhs,
        std::shared_ptr<Expr> rhs,
        Comparison comp);

    virtual ~Inequality();

    // Returns the expression on the left hand side of this inequality.
    const std::shared_ptr<Expr> getLhs() const;

    // Returns the expression on the right hand side of this inequality.
    const std::shared_ptr<Expr> getRhs() const;

    // Returns the comparison of this inequality.
    Comparison getComparison() const;

    // Returns the number of nodes in the ast representing this inequality.
    std::size_t getSize() const;

    // Returns the hash of this inequality.
    std::size_t getHash() const;

    // Returns the set of variables of this inequality.
    const std::set<std::size_t>& getVars() const;

    // Evaluates this inequality w.r.t. the given model.
    bool subs(std::shared_ptr<Model> model) const;

    // Renames all variables in this inequality according to the given
    // substitution map. This instance is not altered, a copy is returned.
    //
    // Throws an OperationError exception if this inequality
    // contains a variable for which no substitution exists in the substitution
    // map.
    std::shared_ptr<Inequality> renameVars(subst_map &subst) const;

    // Returns `0` if this inequality is satisfied by the input model.
    // Otherwise it returns the distance between the value obtained
    // substituting the input model to the lhs of the inequality and
    // a value that would satisfy this inequality.
    //
    // Attention! This method can throw an InvalidOperation exception, in case
    // the evaluation of the model involves in operations with undefined results.
    mpz_class satDelta(std::shared_ptr<Model> model) const;

    // Returns the SMT-LIB v2 representation of this inequality.
    std::string toSmt() const;

    // Returns the SEXPR representation of this inequality.
    std::string toSexpr() const;

    // Returns a copy of this inequality.
    std::shared_ptr<Inequality> copy() const;

    // Calls method `onInequality` of `visitor` passing this inequality to it.
    void visit(FormulaVisitor &visitor) const;
};


// A Clause, that is, a set of inequalities.
class Clause {
private:
    std::vector<std::shared_ptr<Inequality> > ineqs;
    std::set<std::size_t> vars;
    std::size_t size;
    std::size_t hash;

public:
    Clause(std::vector<std::shared_ptr<Inequality> > &ineqs);
    virtual ~Clause();

    // Returns the inequalities this clause is composed of.
    const std::vector<std::shared_ptr<Inequality> >& getIneqs() const;

    // Returns the number of nodes in the ast representing this clause.
    std::size_t getSize() const;

    // Returns the hash of this clause.
    std::size_t getHash() const;

    // Returns the set of variables of this clause.
    const std::set<std::size_t>& getVars() const;

    // Returns `true` iff `model` satisfies at least one inequality in this clause.
    bool subs(std::shared_ptr<Model> model) const;

    // Renames all variables in this clause according to the given substitution
    // map. This instance is not altered, a copy is returned.
    //
    // Throws an OperationError exception if this clause contains a
    // variable for which no substitution exists in the substitution map.
    std::shared_ptr<Clause> renameVars(subst_map &subst) const;

    // Returns the number of inequalities satisfied by the given model.
    std::size_t countSatisfiedInequalities(std::shared_ptr<Model> model) const;

    // Returns the minimum sat-delta of the inequalities this clause is composed of.
    mpz_class satDelta(std::shared_ptr<Model> model) const;

    // Returns the SMT-LIB v2 representation of this clause.
    std::string toSmt() const;

    // Returns the SEXPR representation of this clause.
    std::string toSexpr() const;

    // Returns a copy of this clause.
    std::shared_ptr<Clause> copy() const;

    // Calls method `onClause` of `visitor` passing this clause to it.
    void visit(FormulaVisitor &visitor) const;
};


// A Formula, that is, a set of clauses.
class Formula {
private:
    std::vector<std::shared_ptr<Clause> > clauses;
    std::set<std::size_t> vars;
    std::set<std::size_t> clauses_hashset;
    std::size_t size;
    std::size_t hash;
    
    // Returns all independent subformulas of this formula.
    //
    // This method works by creating a graph in which each node represents a
    // clause of this formula.
    void sliceByClauses(std::vector<std::shared_ptr<Formula> > &result) const;
    
    // Returns all independent subformulas of this formula.
    //
    // This method works by creating a graph in which each node represents a
    // variable of this formula.
    void sliceByVars(std::vector<std::shared_ptr<Formula> > &result) const;

public:
    Formula(std::vector<std::shared_ptr<Clause> > &clauses);
    virtual ~Formula();

    // Returns the clauses of this formula.
    const std::vector<std::shared_ptr<Clause> >& getClauses() const;

    // Returns the `i`-th clause of this formula.
    std::shared_ptr<Clause> getClause(std::size_t i) const;

    // Returns the set of the hashes of the clauses of this formula.
    const std::set<std::size_t>& getClausesHashSet() const;

    // Returns the number of nodes in the ast representing this formula.
    std::size_t getSize() const;

    // Returns the hash of this formula.
    std::size_t getHash() const;
    
    // Returns the set of variables of this formula.
    const std::set<std::size_t>& getVars() const;

    // Renames all variables in this formula according to the given
    // substitution map. This instance is not altered, a copy is returned.
    //
    // Throws an OperationError exception if this formula contains
    // a variable for which no substitution exists in the substitution map.
    std::shared_ptr<Formula> renameVars(subst_map &subst) const;

    // Returns a copy of this formula in which all variables have been mapped
    // to the interval [0,nr_vars) without altering the meaning of this formula.
    std::shared_ptr<Formula> compress() const;

    // Returns all independent subformulas of this formula.
    void slice(std::vector<std::shared_ptr<Formula> > &result) const;

    // Returns `true` iff the given model satisfies all clauses in this formula.
    bool sharesModel(std::shared_ptr<Model> model) const;

    // Returns `true` iff the given unsat-core is contained in this formula.
    bool sharesUnsatCore(std::shared_ptr<UnsatCore> core) const;

    // Returns the number of clauses satisfied by the input model.
    std::size_t countSatisfiedClauses(std::shared_ptr<Model> model) const;

    // Returns the number of inequalities satisfied by the input model.
    std::size_t countSatisfiedInequalities(std::shared_ptr<Model> model) const;

    // Returns the sum of the sat-delta of all clauses this formula is composed of.
    double satDelta(std::shared_ptr<Model> model) const;
    
    // Returns the sum of the sat-delta of all clauses this formula is composed of
    // multiplied by the number of clauses not satisfied by `model`.
    double normalizedSatDelta(std::shared_ptr<Model> model) const;

    // Returns the SMT-LIB v2 representation of this formula.
    std::string toSmt() const;

    // Returns the SEXPR representation of this formula.
    std::string toSexpr() const;

    // Returns a copy of this formula.
    std::shared_ptr<Formula> copy() const;

    // Calls the method `onClause` of `visitor` on each clause in this formula.
    void visit(FormulaVisitor &visitor) const;
};


class FormulaVisitor {
public:
    virtual void onClause(const Clause &clause) = 0;
    virtual void onInequality(const Inequality &inequality) = 0;
    virtual void onAdd(const Add &add) = 0;
    virtual void onMul(const Mul &mul) = 0;
    virtual void onVar(const Var &v) = 0;
    virtual void onConst(const Const &c) = 0;
};

#endif
