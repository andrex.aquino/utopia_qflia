#ifndef CORE_CACHE_CACHE_H
#define CORE_CACHE_CACHE_H

#include <memory>

#include "core/formula/formula.h"


// The abstract concept of cache.
// The template should be instatiated with either `Model` or `UnsatCore`.
template <typename T>
class Cache {
  protected:
    std::shared_ptr<T> solution;

    // Updates the solution stored by this cache.
    void setSolution(std::shared_ptr<T> solution) {
        this->solution = solution;
    };

  public:
    virtual ~Cache() {};

    // Returns the solution currently stored by this cache.
    std::shared_ptr<T> getSolution() const {
        return this->solution;
    };

    // Searches `formula` in the cache and returns `true` if it is found.
    // If the formula is found then its solution is stored and can be
    // retrieved by calling `getSolution`.
    virtual bool search(std::shared_ptr<Formula> formula) = 0;
};

#endif
