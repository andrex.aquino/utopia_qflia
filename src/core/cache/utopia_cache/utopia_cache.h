#ifndef CORE_CACHE_UTOPIA_CACHE_UTOPIA_CACHE_H
#define CORE_CACHE_UTOPIA_CACHE_UTOPIA_CACHE_H

#include "core/cache/cache.h"

#include <cmath> // TODO -- check if this library is really needed.
#include <cstddef>
#include <memory>
#include <sstream>
#include <string>
#include <queue>

#include <boost/algorithm/string.hpp> // includes `boost::erase_all`.

#include "core/formula/formula.h"


// An entry stored in the cache.
// The template should be instantiated with either `Model` or `UnsatCore`.
template <typename T>
class Entry {
	private:
		std::shared_ptr<Formula> formula;
		std::shared_ptr<T> solution;
		double score;

	public:
		Entry(
            std::shared_ptr<Formula> formula,
            std::shared_ptr<T> solution,
            double score)
            : formula(formula),
              solution(solution),
              score(score) {};
		~Entry() {};

		// Returns the formula stored in this entry.
		std::shared_ptr<Formula> getFormula() const {
            return this->formula;
        };

		// Returns the solution stored in this entry.
		std::shared_ptr<T> getSolution() const {
            return this->solution;
        };

		// Returns the score associated to this entry.
		double getScore() const {
            return this->score;
        };

        // Returns a JSON string describing this entry.
		std::string toString() const {
            std::stringstream result;
            std::string formula_s = this->formula->toSmt();
            boost::erase_all(formula_s, "\n");
            result
                << "{"
                << "\"formula\": \"" << formula_s << "\", "
                << "\"solution\": \"" << this->solution->toString() << "\", "
                << "\"score\": " << this->score
                << "}";
            return result.str();
        };
};


// A structure to sort entries according to their score.
template <typename T>
struct OrderByScore {
    bool operator()(
            const Entry<T> &e1,
            const Entry<T> &e2) {
        return (e1.getScore() < e2.getScore());
    };
};


// The UToPiA cache.
// The template should be instantiated with either `Model` or `UnsatCore`.
template <typename T>
class UtopiaCache: public Cache<T> {
  private:
    // The reference point used to calculate the score of formulas on searches.
    std::shared_ptr<Formula> reference_point;

    // The repository of this cache as an ordered multiset of pairs <formula, score>.
    std::multiset<Entry<T>, OrderByScore<T> > entries;
    
    // Number of candidates extracted from the cache during the first tier of the search process.
    std::size_t tier1_candidates;

    // Number of candidates extracted from the cache during the second tier of the search process.
    std::size_t tier2_candidates;

    // Loads in `result` the `nr_tier1_candidates` entries the score of which is the closest to `score`.
    void tier1Search(
    		std::shared_ptr<Formula> formula,
            std::vector<Entry<T> > &result) {
        // Get the distance of the current formula from the reference point.
        double score = this->tier1Heuristic(formula, this->reference_point);

        // If the cache is empty or no candidates are requested, do nothing.
        if (this->tier1_candidates <= 0 || this->entries.size() <= 0) {
            return;
        }

        // Find the entry in this cache that is the closest to the given score.
        // To do so, create a fake entry that only contains the score of the target formula.
        Entry<T> fake_entry(nullptr, nullptr, score);

        typename std::multiset<Entry<T>, OrderByScore<T> >::iterator closest_it;
        auto it = this->entries.lower_bound(fake_entry);
        if (it == this->entries.end()) {
            closest_it = std::prev(it);
        } else if (it == this->entries.begin()) {
            closest_it = it;
        } else {
            double delta_curr = std::fabs(it->getScore() - score);
            double delta_prev = std::fabs(std::prev(it)->getScore() - score);
            closest_it = (delta_curr <= delta_prev)?(it):(std::prev(it));
        }

        // Extract `k` elements from the multiset by proximity to `score`.
        typename std::multiset<Entry<T>, OrderByScore<T> >::iterator left_it;
        typename std::multiset<Entry<T>, OrderByScore<T> >::iterator right_it;

        bool prev_out = false;
        if (closest_it != this->entries.begin()) {
            left_it = std::prev(closest_it);
        } else {
            prev_out = true;
        }

        right_it = std::next(closest_it);
        result.push_back(*closest_it);
        std::size_t k = std::min(this->entries.size() - 1, this->tier1_candidates - 1);
        for (unsigned long i=0; i<k; ++i) {
            if (prev_out and right_it == this->entries.end()) {
                break;
            } else if (prev_out) {
                result.push_back(*right_it);
                ++right_it;
            } else if (right_it == this->entries.end()) {
                result.push_back(*left_it);
                if (left_it != this->entries.begin()) {
                    --left_it;
                } else {
                    prev_out = true;
                }
            } else {
                double left_delta = std::fabs(left_it->getScore() - score);
                double right_delta = std::fabs(right_it->getScore() - score);
                if (left_delta <= right_delta) {
                    result.push_back(*left_it);
                    if (left_it != this->entries.begin()) {
                        --left_it;
                    } else {
                        prev_out = true;
                    }
                } else {
                    result.push_back(*right_it);
                    ++right_it;
                }
            }
        }
    };

    // Loads in `result` the `nr_tier2_candidates` entries with the minimum score w.r.t. `formula`.
    void tier2Search(
    		std::shared_ptr<Formula> formula,
    		std::vector<Entry<T> > &candidates,
            std::vector<Entry<T> > &result) {
        
        typedef std::pair<Entry<T>, double> marked_entry;

        struct comparator {
                bool operator()(
                        const marked_entry &p1,
                        const marked_entry &p2) {
                    return (p1.second < p2.second);
                }
        };

        std::priority_queue<marked_entry, std::vector<marked_entry>, comparator> max_heap;
        for (auto it=candidates.begin(); it!=candidates.end(); ++it) {
            double score = this->tier2Heuristic(formula, it->getFormula());
            max_heap.push(std::make_pair(*it, score));
        }

        std::size_t k = std::min(this->tier2_candidates, candidates.size());
        for (std::size_t i=0; i<k; ++i) {
            auto pair = max_heap.top();
            result.push_back(pair.first);
            max_heap.pop();
        }
    };

  public:
    UtopiaCache() : tier1_candidates(10), tier2_candidates(10) {
        // Set the reference point to an empty formula.
        std::vector<std::shared_ptr<Clause> > no_clauses;
        this->reference_point = std::shared_ptr<Formula>(new Formula(no_clauses));
    };
   	virtual ~UtopiaCache() {};

    // Returns the reference point of this cache.
    std::shared_ptr<Formula> getReferencePoint() const {
        return this->reference_point;
    };

    // Returns the set of entries in this cache.
    const std::multiset<Entry<T>, OrderByScore<T> >& getEntries() const {
        return this->entries;
    };
    
    // Returns the number of candidates retrieved during the first search tier.
    std::size_t getTier1Candidates() {
        return this->tier1_candidates;
    };

    // Returns the number of candidates retrieved during the second search tier.
    std::size_t getTier2Candidates() {
        return this->tier2_candidates;
    };

    // Modifies the reference point of this cache.
    void setReferencePoint(std::shared_ptr<Formula> formula) {
        this->reference_point = formula;
    };

    // Sets the number of candidates retrieved during the first search tier.
    void setTier1Candidates(std::size_t n) {
        this->tier1_candidates = n;
    };

    // Sets the number of candidates retrieved during the second search tier.
    void setTier2Candidates(std::size_t n) {
        this->tier2_candidates = n;
    };

    // Adds an entry to this cache.
    void store(
            std::shared_ptr<Formula> formula,
            std::shared_ptr<T> solution) {
        double score = this->tier1Heuristic(formula, this->reference_point);
        Entry<T> entry(formula, solution, score);
        this->entries.insert(entry);
    };

    // Abstract method used to calculate the distance of the target formula from the reference point.
    virtual double tier1Heuristic(
        std::shared_ptr<Formula> f1,
        std::shared_ptr<Formula> f2) const = 0;

    // Abstract method used to calculate the distance of the target formula from other formulas.
    virtual double tier2Heuristic(
        std::shared_ptr<Formula> f1,
        std::shared_ptr<Formula> f2) const = 0;

    // Searches `formula` in the cache using the 2 tiers selection process,
    // returning `true` if the search is successful.
    bool search(std::shared_ptr<Formula> formula) {
        // Extract from the formula the `nr_candidates` candidate formulas
        // the score of which is the closest to score_rp.
        std::vector<Entry<T> > tier1_candidates;
        this->tier1Search(formula, tier1_candidates);
        
        // Extract the `nr_tier2_candidates` entries with minimal distance from the target formula.
        std::vector<Entry<T> > tier2_candidates;
        this->tier2Search(formula, tier1_candidates, tier2_candidates);

        // Try to reuse the extracted solutions.
        for (auto it=tier2_candidates.begin(); it!=tier2_candidates.end(); ++it) {
            std::shared_ptr<T> solution = it->getSolution();
            if (solution->check(formula)) {
                this->setSolution(solution);
                return true;
            }
        }
        
        this->setSolution(nullptr);
        return false;
    };
};

#endif
