#ifndef CORE_CACHE_UTOPIA_CACHE_CONST_CACHE_CONST_CACHE_H
#define CORE_CACHE_UTOPIA_CACHE_CONST_CACHE_CONST_CACHE_H

#include <memory>
#include <stdlib.h>

#include "core/formula/formula.h"
#include "core/cache/utopia_cache/utopia_cache.h"


template <typename T>
class ConstCache: public UtopiaCache<T> {
  public:
    ConstCache() {}
    
    // Returns `0`.
    double tier1Heuristic(
    		std::shared_ptr<Formula> f1,
            std::shared_ptr<Formula> f2) const {
        
        return 0.0;
    };

    // Returns `0`.
    double tier2Heuristic(
    		std::shared_ptr<Formula> f1,
            std::shared_ptr<Formula> f2) const {
        
        return 0.0;
    };
};

#endif
