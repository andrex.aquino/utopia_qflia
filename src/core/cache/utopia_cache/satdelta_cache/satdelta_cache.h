#ifndef CORE_CACHE_UTOPIA_CACHE_SATDELTA_CACHE_SATDELTA_CACHE_H
#define CORE_CACHE_UTOPIA_CACHE_SATDELTA_CACHE_SATDELTA_CACHE_H

#include <cstddef>
#include <gmpxx.h>
#include <map>
#include <memory>

#include "core/formula/formula.h"
#include "core/cache/utopia_cache/utopia_cache.h"

// #define USE_NORMALIZED_SATDELTA


template <typename T>
class SatDeltaCache: public UtopiaCache<T> {
  protected:
    std::vector<std::shared_ptr<Model> > models; // Reference models.
    std::map<std::size_t, double> hash_map;
  
    void addConstModel(mpz_class v) {
        // @ATTENTION -- Works for formulas of up to 1000 variables.
        std::map<std::size_t, mpz_class> map;
        for (std::size_t i=0; i<1000; ++i) {
            map[i] = v;
        }
        std::shared_ptr<Model> model(new Model(map));
        this->models.push_back(model);
    }

    double getDelta(std::shared_ptr<Formula> f) const {
        std::size_t hash = f->getHash();
        
        auto it = this->hash_map.find(hash);
        if (it != this->hash_map.end()) {
            return it->second;
        }
        
        double result = 0;
        for (auto it=this->models.begin(); it!=this->models.end(); ++it) {
#ifdef USE_NORMALIZED_SATDELTA
            result += f->normalizedSatDelta(*it);
#else
            result += f->satDelta(*it);
#endif
        }
        result /= static_cast<double>(this->models.size());
        const_cast<SatDeltaCache*>(this)->hash_map[hash] = result;
        return result;
    }

  public:
    SatDeltaCache() {}
  
    double tier1Heuristic(
    		std::shared_ptr<Formula> f1,
            std::shared_ptr<Formula> f2) const {
        
        return this->getDelta(f1);
    };

    // Returns the sat-delta similarity of two formulas.
    double tier2Heuristic(
    		std::shared_ptr<Formula> f1,
            std::shared_ptr<Formula> f2) const {
        
        double f1_delta = this->getDelta(f1);
        double f2_delta = this->getDelta(f2);
        return -1.0 * std::fabs(f1_delta - f2_delta);
    };
};


// Single model based sat-delta caches.

template<typename T>
class SatDeltaCacheA1: public SatDeltaCache<T> {
  public:
    SatDeltaCacheA1() {
        this->addConstModel(0);
    }
};

template<typename T>
class SatDeltaCacheB1: public SatDeltaCache<T> {
  public:
    SatDeltaCacheB1() {
        this->addConstModel(1);
    }
};

template<typename T>
class SatDeltaCacheC1: public SatDeltaCache<T> {
  public:
    SatDeltaCacheC1() {
        this->addConstModel(2);
    }
};

template<typename T>
class SatDeltaCacheD1: public SatDeltaCache<T> {
  public:
    SatDeltaCacheD1() {
        this->addConstModel(-1);
    }
};

template<typename T>
class SatDeltaCacheE1: public SatDeltaCache<T> {
  public:
    SatDeltaCacheE1() {
        this->addConstModel(-2);
    }
};


// Three model based sat-delta caches.

template<typename T>
class SatDeltaCacheA3: public SatDeltaCache<T> {
  public:
    SatDeltaCacheA3() {
        this->addConstModel(-1);
        this->addConstModel(0);
        this->addConstModel(1);
    }
};

template<typename T>
class SatDeltaCacheB3: public SatDeltaCache<T> {
  public:
    SatDeltaCacheB3() {
        this->addConstModel(-1000);
        this->addConstModel(0);
        this->addConstModel(1000);
    }
};

// This distance is based on the non symmetry of the three models.
template<typename T>
class SatDeltaCacheC3: public SatDeltaCache<T> {
  public:
    SatDeltaCacheC3() {
        this->addConstModel(-10000);
        this->addConstModel(0);
        this->addConstModel(100);
    }
};


// Five model based sat-delta caches.

template<typename T>
class SatDeltaCacheA5: public SatDeltaCache<T> {
  public:
    SatDeltaCacheA5() {
        this->addConstModel(-2);
        this->addConstModel(-1);
        this->addConstModel(0);
        this->addConstModel(1);
        this->addConstModel(2);
    }
};

template<typename T>
class SatDeltaCacheB5: public SatDeltaCache<T> {
  public:
    SatDeltaCacheB5() {
        this->addConstModel(-10000);
        this->addConstModel(-100);
        this->addConstModel(0);
        this->addConstModel(100);
        this->addConstModel(10000);
    }
};


// Nine model based sat-delta caches.

template<typename T>
class SatDeltaCacheA9: public SatDeltaCache<T> {
  public:
    SatDeltaCacheA9() {
        this->addConstModel(-10000);
        this->addConstModel(-1000);
        this->addConstModel(-100);
        this->addConstModel(-10);
        this->addConstModel(0);
        this->addConstModel(10);
        this->addConstModel(100);
        this->addConstModel(1000);
        this->addConstModel(10000);
    }
};

template<typename T>
class SatDeltaCacheB9: public SatDeltaCache<T> {
  public:
    SatDeltaCacheB9() {
        this->addConstModel(-4);
        this->addConstModel(-3);
        this->addConstModel(-2);
        this->addConstModel(-1);
        this->addConstModel(0);
        this->addConstModel(1);
        this->addConstModel(2);
        this->addConstModel(3);
        this->addConstModel(4);
    }
};

#endif
