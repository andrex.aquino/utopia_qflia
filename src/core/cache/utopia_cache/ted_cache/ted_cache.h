#ifndef CORE_CACHE_UTOPIA_CACHE_TED_CACHE_TED_CACHE_H
#define CORE_CACHE_UTOPIA_CACHE_TED_CACHE_TED_CACHE_H

#include <memory>

#include "commons/ted/zsted.h"
#include "core/formula/formula.h"
#include "core/cache/utopia_cache/utopia_cache.h"


std::shared_ptr<Node> exprToTree(std::shared_ptr<Expr> expr) {
    std::shared_ptr<Const> c = std::dynamic_pointer_cast<Const>(expr);
    if (c != nullptr) {
        std::shared_ptr<Node> tree(new Node("const"));
        std::stringstream ss;
        ss << c->getValue();
        tree->addChild(std::shared_ptr<Node>(new Node(ss.str())));
        return tree;
    }
    
    std::shared_ptr<Var> v = std::dynamic_pointer_cast<Var>(expr);
    if (v != nullptr) {
        std::shared_ptr<Node> tree(new Node("var"));
        tree->addChild(std::shared_ptr<Node>(new Node(std::to_string(v->getIndex()))));
        return tree;
    }
    
    std::shared_ptr<Add> add = std::dynamic_pointer_cast<Add>(expr);
    if (add != nullptr) {
        std::shared_ptr<Node> tree(new Node("add"));
        tree->addChild(exprToTree(add->getLhs()));
        tree->addChild(exprToTree(add->getRhs()));
        return tree;
    }
    
    std::shared_ptr<Mul> mul = std::dynamic_pointer_cast<Mul>(expr);
    if (mul != nullptr) {
        std::shared_ptr<Node> tree(new Node("mul"));
        tree->addChild(exprToTree(mul->getConst()));
        tree->addChild(exprToTree(mul->getExpr()));
        return tree;
    }
    
    // This should never happen.
    assert(false);
}

std::shared_ptr<Node> inequalityToTree(std::shared_ptr<Inequality> ineq) {
    std::shared_ptr<Node> tree;
    switch (ineq->getComparison()) {
        case Comparison::LT:
            tree = std::shared_ptr<Node>(new Node("lt"));
        break;
        case Comparison::LE:
            tree = std::shared_ptr<Node>(new Node("le"));
        break;
        case Comparison::GT:
            tree = std::shared_ptr<Node>(new Node("gt"));
        break;
        case Comparison::GE:
            tree = std::shared_ptr<Node>(new Node("ge"));
        break;
        case Comparison::EQ:
            tree = std::shared_ptr<Node>(new Node("eq"));
        break;
        case Comparison::NE:
            tree = std::shared_ptr<Node>(new Node("ne"));
        break;
    }
    tree->addChild(exprToTree(ineq->getLhs()));
    tree->addChild(exprToTree(ineq->getRhs()));
    return tree;
}

std::shared_ptr<Node> clauseToTree(std::shared_ptr<Clause> clause) {
    std::shared_ptr<Node> tree(new Node("clause"));
    const std::vector<std::shared_ptr<Inequality> > &ineqs = clause->getIneqs();
    for (auto it=ineqs.begin(); it!=ineqs.end(); ++it) {
        tree->addChild(inequalityToTree(*it));
    }
    return tree;
}

std::shared_ptr<Node> formulaToTree(std::shared_ptr<Formula> formula) {
    std::shared_ptr<Node> tree(new Node("formula"));
    const std::vector<std::shared_ptr<Clause> > &clauses = formula->getClauses();
    for (auto it=clauses.begin(); it!=clauses.end(); ++it) {
        tree->addChild(clauseToTree(*it));
    }
    return tree;
}


// Functions to calculate the cost of edit operations.

int labelDistance(std::string s1, std::string s2) {
    if (s1 == s2) {
        return 0;
    }
    return 1;
}

int insertCost(std::shared_ptr<Node> node) {
    return labelDistance("", node->getLabel());
}

int removeCost(std::shared_ptr<Node> node) {
    return labelDistance(node->getLabel(), "");
}

int updateCost(std::shared_ptr<Node> node1, std::shared_ptr<Node> node2) {
    return labelDistance(node1->getLabel(), node2->getLabel());
}


// Ted cache.

template <typename T>
class TedCache: public UtopiaCache<T> {
	public:
    // TODO -- implement.
    double tier1Heuristic(
    		std::shared_ptr<Formula> f1,
            std::shared_ptr<Formula> f2) const {
        
        return 0.0;
    };

    double tier2Heuristic(
    		std::shared_ptr<Formula> f1,
            std::shared_ptr<Formula> f2) const {
        
        std::shared_ptr<Node> tree1 = formulaToTree(f1);
        std::shared_ptr<Node> tree2 = formulaToTree(f2);
        
        int ted = distance(
            tree1,
            tree2,
            insertCost,
            removeCost,
            updateCost
        );
        return -ted;
    };
};

#endif
