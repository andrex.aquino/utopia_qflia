#ifndef CORE_CACHE_UTOPIA_CACHE_JACCARD_CACHE_JACCARD_CACHE_H
#define CORE_CACHE_UTOPIA_CACHE_JACCARD_CACHE_JACCARD_CACHE_H

#include <memory>

#include "core/formula/formula.h"
#include "core/cache/utopia_cache/utopia_cache.h"


template <typename T>
class JaccardCache: public UtopiaCache<T> {
  public:
    double tier1Heuristic(
    		std::shared_ptr<Formula> f1,
            std::shared_ptr<Formula> f2) const {
        
        return f1->getSize();
    };

    // Returns the Jaccard similarity of the sets of clauses of two formulas.
    double tier2Heuristic(
    		std::shared_ptr<Formula> f1,
            std::shared_ptr<Formula> f2) const {
        
        const std::set<unsigned long> &f1_chs = f1->getClausesHashSet();
        const std::set<unsigned long> &f2_chs = f2->getClausesHashSet();
        
        if (f1_chs.empty() || f2_chs.empty()) {
            return 0.0;
        }

        auto it1 = f1_chs.begin();
        auto it1_end = f1_chs.end();
        auto it2 = f2_chs.begin();
        auto it2_end = f2_chs.end();
        
        if (*it1 > *f2_chs.rbegin() || *it2 > *f1_chs.rbegin()) {
            return 0.0;
        }

        double intersection_size = 0.0;
        while(it1 != it1_end && it2 != it2_end) {
            if (*it1 == *it2) {
                ++intersection_size;
                ++it1;
                ++it2;
            } else if (*it1 < *it2) {
                ++it1;
            } else {
                ++it2;
            }
        }
        
        double union_size = 0;
        union_size += (double)f1_chs.size();
        union_size += (double)f2_chs.size();
        union_size -= intersection_size;
        
        return (intersection_size / union_size);
    };
};

#endif
