#ifndef CORE_CACHE_UTOPIA_CACHE_SATCLAUSES_CACHE_SATCLAUSES_CACHE_H
#define CORE_CACHE_UTOPIA_CACHE_SATCLAUSES_CACHE_SATCLAUSES_CACHE_H

#include <cstddef>
#include <gmpxx.h>
#include <map>
#include <memory>

#include "core/formula/formula.h"
#include "core/cache/utopia_cache/utopia_cache.h"


template <typename T>
class SatClausesCache: public UtopiaCache<T> {
  protected:
    std::vector<std::shared_ptr<Model> > models; // Reference models.
    std::map<std::size_t, double> hash_map;
  
    void addConstModel(mpz_class v) {
        // @ATTENTION -- Works for formulas of up to 1000 variables.
        std::map<std::size_t, mpz_class> map;
        for (std::size_t i=0; i<1000; ++i) {
            map[i] = v;
        }
        std::shared_ptr<Model> model(new Model(map));
        this->models.push_back(model);
    }

    double getDelta(std::shared_ptr<Formula> f) const {
        std::size_t hash = f->getHash();
        
        auto it = this->hash_map.find(hash);
        if (it != this->hash_map.end()) {
            return it->second;
        }
        
        double result = 0;
        for (auto it=this->models.begin(); it!=this->models.end(); ++it) {
            double delta = static_cast<double>(f->countSatisfiedClauses(*it));
            delta /= static_cast<double>(f->getClauses().size());
            result += delta;
        }
        result /= static_cast<double>(this->models.size());
        const_cast<SatClausesCache*>(this)->hash_map[hash] = result;
        return result;
    }

  public:
    SatClausesCache() {}
  
    double tier1Heuristic(
    		std::shared_ptr<Formula> f1,
            std::shared_ptr<Formula> f2) const {
        
        return this->getDelta(f1);
    };

    // Returns the sat-clauses similarity of two formulas.
    double tier2Heuristic(
    		std::shared_ptr<Formula> f1,
            std::shared_ptr<Formula> f2) const {
        
        double f1_delta = this->getDelta(f1);
        double f2_delta = this->getDelta(f2);
        return -1.0 * std::fabs(f1_delta - f2_delta);
    };
};


// Single model based sat-clauses caches.

template<typename T>
class SatClausesCacheA1: public SatClausesCache<T> {
  public:
    SatClausesCacheA1() {
        this->addConstModel(0);
    }
};

template<typename T>
class SatClausesCacheB1: public SatClausesCache<T> {
  public:
    SatClausesCacheB1() {
        this->addConstModel(1);
    }
};

template<typename T>
class SatClausesCacheC1: public SatClausesCache<T> {
  public:
    SatClausesCacheC1() {
        this->addConstModel(2);
    }
};

template<typename T>
class SatClausesCacheD1: public SatClausesCache<T> {
  public:
    SatClausesCacheD1() {
        this->addConstModel(-1);
    }
};

template<typename T>
class SatClausesCacheE1: public SatClausesCache<T> {
  public:
    SatClausesCacheE1() {
        this->addConstModel(-2);
    }
};


// Three model based sat-clauses caches.

template<typename T>
class SatClausesCacheA3: public SatClausesCache<T> {
  public:
    SatClausesCacheA3() {
        this->addConstModel(-1);
        this->addConstModel(0);
        this->addConstModel(1);
    }
};

template<typename T>
class SatClausesCacheB3: public SatClausesCache<T> {
  public:
    SatClausesCacheB3() {
        this->addConstModel(-1000);
        this->addConstModel(0);
        this->addConstModel(1000);
    }
};

// This distance is based on the non symmetry of the three models.
template<typename T>
class SatClausesCacheC3: public SatClausesCache<T> {
  public:
    SatClausesCacheC3() {
        this->addConstModel(-10000);
        this->addConstModel(0);
        this->addConstModel(100);
    }
};


// Five model based sat-clauses caches.

template<typename T>
class SatClausesCacheA5: public SatClausesCache<T> {
  public:
    SatClausesCacheA5() {
        this->addConstModel(-2);
        this->addConstModel(-1);
        this->addConstModel(0);
        this->addConstModel(1);
        this->addConstModel(2);
    }
};

template<typename T>
class SatClausesCacheB5: public SatClausesCache<T> {
  public:
    SatClausesCacheB5() {
        this->addConstModel(-10000);
        this->addConstModel(-100);
        this->addConstModel(0);
        this->addConstModel(100);
        this->addConstModel(10000);
    }
};


// Nine model based sat-clauses caches.

template<typename T>
class SatClausesCacheA9: public SatClausesCache<T> {
  public:
    SatClausesCacheA9() {
        this->addConstModel(-10000);
        this->addConstModel(-1000);
        this->addConstModel(-100);
        this->addConstModel(-10);
        this->addConstModel(0);
        this->addConstModel(10);
        this->addConstModel(100);
        this->addConstModel(1000);
        this->addConstModel(10000);
    }
};

template<typename T>
class SatClausesCacheB9: public SatClausesCache<T> {
  public:
    SatClausesCacheB9() {
        this->addConstModel(-4);
        this->addConstModel(-3);
        this->addConstModel(-2);
        this->addConstModel(-1);
        this->addConstModel(0);
        this->addConstModel(1);
        this->addConstModel(2);
        this->addConstModel(3);
        this->addConstModel(4);
    }
};

#endif
