#ifndef COMMONS_UTILS_MATRIX_MATRIX_H
#define COMMONS_UTILS_MATRIX_MATRIX_H


// A dynamically allocated bidimensional array.
class Matrix {
  private:
    int rows;
    int cols;
    int *data;
    
    // Maps integer i to the i-th element of the row of the matrix, if i is
    // negative the index to the i-th to last element of the row is returned.
    int mapRow(int i) const;
    
    // Maps integer i to the i-th element of the column of the matrix, if i is
    // negative the index to the i-th to last element of the column is returned.
    int mapColumn(int j) const;
    
    // Returns the index of the cell (i,j) in the matrix represented as a vector.
    int index(int i, int j) const;
    
  public:
    // Creates a matrix with `rows` rows and `cols` columns in which all
    // cells are initialized to zero.
    Matrix(int rows, int cols);
    
    // Frees the memory allocated when the matrix was created.
    ~Matrix();
    
    // Returns the element in the cell (i,j).
    int get(int i, int j) const;
    
    // Modifies the element in the cell (i,j) settings it to `value`.
    void set(int i, int j, int value);
};

#endif