#ifndef COMMONS_UTILS_SET_H
#define COMMONS_UTILS_SET_H

#include <set>


template <typename T>
bool disjoint(const std::set<T> &s1, const std::set<T> &s2) {
    if (s1.empty() || s2.empty()) {
        return true;
    }

    auto it1 = s1.begin();
    auto it1_end = s1.end();
    auto it2 = s2.begin();
    auto it2_end = s2.end();
    
    if (*it1 > *s2.rbegin() || *it2 > *s1.rbegin()) {
        return true;
    }

    while (it1 != it1_end && it2 != it2_end) {
        if (*it1 == *it2) {
            return false;
        } else if (*it1 < *it2) {
            ++it1;
        } else {
            ++it2;
        }
    }

	return true;
}


template <typename T>
bool is_subset(const std::set<T> &s1, const std::set<T> &s2) {
    if (s1.empty()) {
        return true;
    }
    
    if (s2.empty()) {
        return false;
    }

    auto it1 = s1.begin();
    auto it1_end = s1.end();
    auto it2 = s2.begin();
    auto it2_end = s2.end();
    
    if (*it1 > *s2.rbegin() || *it2 > *s1.rbegin()) {
        return false;
    }

    while (it1 != it1_end) {
        while (*it2 < *it1 && it2 != it2_end) {
            ++it2;
        }
        
        if (it2 == it2_end || *it2 > *it1) {
            return false;
        }
        
        ++it1;
        ++it2;
    }

	return true;
}

#endif
