#ifndef COMMONS_TIMER_TIMER_H_
#define COMMONS_TIMER_TIMER_H_

#include <chrono>


// An object to time the execution of code blocks.
//
// Sample usage:
//    Timer t;
//    t.start();
//    // do stuff...
//    auto time_elapsed = t.micro();
class Timer {
  private:
    std::chrono::high_resolution_clock::time_point tp;

  public:
    Timer();
    virtual ~Timer();

    // Starts the timer.
    void start();
    
    // Returns the time elapsed from the last call to start in milliseconds.
    std::chrono::milliseconds::rep milli();
    
    // Returns the time elapsed from the last call to start in microseconds.
    std::chrono::microseconds::rep micro();
    
    // Returns the time elapsed from the last call to start in nanoseconds.
    std::chrono::nanoseconds::rep nano();
};

#endif