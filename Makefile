OPENMP=

BOOST_INCLUDES=-I../lib/boost
BOOST_LIBS=-L../lib/boost/stage/lib
GMP_INCLUDES=-I../lib/gmp-6.1.0
GMP_LIBS=-L../lib/gmp-6.1.0/.libs
Z3_INCLUDES=-I../lib/z3/src/api -I../lib/z3/src/api/c++
Z3_LIBS=-L../lib/z3/build

FILES=$(shell find ./src -type f -name "*.cpp" | grep -v "^./src/tools")
FILES_OBJECTS=${FILES:.cpp=.o}

TOOLS_NAMES = $(shell find ./src/tools -type f -name "*.cpp" | sed 's/\.\/src\/tools\///g' | sed 's/\.cpp//g')
TOOLS=${TOOLS_NAMES:%=./src/tools/%.cpp}
TOOLS_OBJECTS=${TOOLS_NAMES:%=./src/tools/%.o}
TOOLS_BINARIES=${TOOLS_NAMES:%=./bin/%}


# ===============
# | Phony rules |
# ===============

.PHONY: all
all: $(TOOLS_BINARIES)

.PHONY: clean
clean:
	find ./src -name "*.o" -exec rm {} +

.PHONY: cleanall
cleanall: clean
	rm -f ./bin/*


# ===============
# | Compilation |
# ===============

$(TOOLS_OBJECTS): $(TOOLS)
	g++ $(OPENMP) -Wall -std=c++11 -I./src $(Z3_INCLUDES) $(GMP_INCLUDES) $(BOOST_INCLUDES) -c $(subst .o,.cpp,$@) -o $@


# ===========
# | Linking |
# ===========

$(TOOLS_BINARIES): $(FILES_OBJECTS) $(TOOLS_OBJECTS)
	g++ $(OPENMP) $(Z3_LIBS) $(GMP_LIBS) $(BOOST_LIBS) $(FILES_OBJECTS) $(addsuffix .o, $(subst bin,src/tools,$@)) -o $@ -lz3 -lboost_program_options -lgmpxx -lgmp


# ==============================
# | Compilation of other units |
# ==============================

%.o: %.cpp
	g++ $(OPENMP) -Wall -std=c++11 -I./src $(Z3_INCLUDES) $(GMP_INCLUDES) $(BOOST_INCLUDES) -c ./$< -o $@
